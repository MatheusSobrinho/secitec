package br.com.secitec.pessoa.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.secitec.pessoa.entities.Address;
import br.com.secitec.pessoa.repositories.AddressRepository;
import br.com.secitec.pessoa.services.AddressService;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
    private AddressRepository addressRepository;
	
	@Override
	public Address save(Address address) {
		return addressRepository.save(address);
	}

	@Override
	public Address update(Address address) {
		return addressRepository.save(address);
	}

	@Override
	public void delete(Address address) {
		addressRepository.delete(address);
	}

	@Override
	public Optional<Address> findById(Integer id) {
		return addressRepository.findById(id);
	}

	@Override
	public List<Address> listAll() {
		return addressRepository.findAll();
	}
}
