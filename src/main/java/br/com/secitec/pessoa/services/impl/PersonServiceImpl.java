package br.com.secitec.pessoa.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.secitec.pessoa.entities.Person;
import br.com.secitec.pessoa.repositories.PersonRepository;
import br.com.secitec.pessoa.services.PersonService;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
    private PersonRepository personRepository;
	
	@Override
	public Person save(Person person) {
		return personRepository.save(person);
	}

	@Override
	public Person update(Person person) {
		return personRepository.save(person);
	}

	@Override
	public void delete(Person person) {
		personRepository.delete(person);
	}

	@Override
	public Optional<Person> findById(Long id) {
		return personRepository.findById(id);
	}

	@Override
	public List<Person> listAll() {
		return personRepository.findAll();
	}

	@Override
	public Optional<Person> findByCpf(String cpf) {
		return personRepository.findByCpf(cpf);
	}
}
