package br.com.secitec.pessoa.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.secitec.pessoa.entities.Person;

@Service
public interface PersonService {
	public Person save(Person person);

	public Person update(Person person);

	public void delete(Person person);

	public Optional<Person> findById(Long id);

	public List<Person> listAll();

	public Optional<Person> findByCpf(String cpf);
}
