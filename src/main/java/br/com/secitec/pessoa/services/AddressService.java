package br.com.secitec.pessoa.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.secitec.pessoa.entities.Address;

@Service
public interface AddressService {
	public Address save(Address address);

	public Address update(Address address);

	public void delete(Address address);

	public Optional<Address> findById(Integer id);

	public List<Address> listAll();
}
