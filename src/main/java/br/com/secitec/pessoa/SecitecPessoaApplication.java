package br.com.secitec.pessoa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecitecPessoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecitecPessoaApplication.class, args);
	}
}
