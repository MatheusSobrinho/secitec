package br.com.secitec.pessoa.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/address")
public class AddressResource {
	
	@GetMapping
	public ResponseEntity<?> teste(){
		return ResponseEntity.ok("address teste");
	}
}
