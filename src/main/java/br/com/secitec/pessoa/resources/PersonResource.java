package br.com.secitec.pessoa.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.secitec.pessoa.entities.Person;
import br.com.secitec.pessoa.services.impl.PersonServiceImpl;

@RestController
@RequestMapping(value = "/person")
public class PersonResource {
	
	@Autowired
	private PersonServiceImpl personServiceImpl;
	
	@GetMapping
	public ResponseEntity<?> teste(){
		List<Person> persons = personServiceImpl.listAll();
		
		return ResponseEntity.ok().body(persons);
	}
	
	
	@PostMapping
	public ResponseEntity<?> savePerson(@Valid @RequestBody Person person, BindingResult bindingResult) throws Exception {

		try {
			if (bindingResult.hasErrors()) {
				List<String> errorList = new ArrayList<String>();
				bindingResult.getAllErrors().forEach(error -> errorList.add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(errorList);
			}
		
			Person personSave = personServiceImpl.save(person);
			
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(personSave.getId()).toUri();
	        
			return ResponseEntity.created(uri).build(); // Padrão de status 201

		} catch (ConstraintViolationException e) {
			return ResponseEntity.badRequest().body("Error: "+e.getMessage());
		} 
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePerson(@PathVariable("id") Long id, @Valid @RequestBody Person personPut,
			BindingResult bindingResult) {
		try {
			if (bindingResult.hasErrors()) {
				List<String> errorList = new ArrayList<String>();
				bindingResult.getAllErrors().forEach(error -> errorList.add(error.getDefaultMessage()));
				return ResponseEntity.badRequest().body(errorList);
			}

			Optional<Person> personExist = personServiceImpl.findById(id);
			personExist.orElseThrow(() -> new Exception("Não existe o ID" + personPut.getId()));
			
			Person person = personExist.get();
			person.setId(id);
			
			personServiceImpl.update(person);
			
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	
	
}
