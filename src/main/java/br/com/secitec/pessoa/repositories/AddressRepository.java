package br.com.secitec.pessoa.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.secitec.pessoa.entities.Address;
import br.com.secitec.pessoa.entities.Person;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{
	public Optional<Address> findById(Integer id);
}
