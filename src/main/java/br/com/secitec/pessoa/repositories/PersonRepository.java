package br.com.secitec.pessoa.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.secitec.pessoa.entities.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer>{
	public Optional<Person> findByCpf(String cpf);

	public Optional<Person> findById(Long id);
}
